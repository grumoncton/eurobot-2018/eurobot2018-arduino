#include <Arduino.h>
#include <Wire.h>
#include "address-map.h"

#define MICROSECONDS_TO_MM(ms) (ms * 16 / 47 / 2)
#define MM_TO_MICROSECONDS(mm) (mm / 16 * 47 * 2)

#define FACTOR (4)
#define MAX_VALUE_MM (255 * FACTOR)
#define MAX_VALUE_MICROSECONDS MM_TO_MICROSECONDS(MAX_VALUE_MM)

int trigPin[] = {2, 4, 6, 8, 10, 12, A0, A2};
int echoPin[] = {3, 5, 7, 9, 11, 13, A1, A3};
int distINT[] = {0, 0, 0, 0, 0, 0, 0, 0};
uint8_t distances_bytes[] = {0, 0, 0, 0, 0, 0, 0, 0};

void dataRequest() {
  switch(Wire.read()) {
    case 0x01: {
      Wire.write(0x00);
      break;
    }

    case 0x03: {
      for (int i = 0; i <= 7; i++) {
        Wire.write(distances_bytes[i]);
      }
      break;
    }
  }
}

uint16_t readUltra(uint8_t i) {

  // Send pulse
  digitalWrite(trigPin[i], LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin[i], HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin[i], LOW);

  // Get distance in
  uint16_t duration = pulseIn(echoPin[i], HIGH, MAX_VALUE_MICROSECONDS);

  // Reset echo pin so it doesn't fuck up
  pinMode(echoPin[i], OUTPUT);
  digitalWrite(echoPin[i], LOW);
  pinMode(echoPin[i], INPUT);

  // If value too big for ultra, return max value
  if (duration == 0) {
    return MAX_VALUE_MM;
  }

  return MICROSECONDS_TO_MM(duration);
}

void setup() {
  Wire.begin(ADDRESS_ULTRA);
  Wire.onRequest(dataRequest);

  for (int i = 0; i <= 7; i++) {
    pinMode(trigPin[i], OUTPUT);
    pinMode(echoPin[i], INPUT);
  }
}

void loop() {
  for (int i = 0; i <= 7; i++) {

    uint16_t distance = readUltra(i);

    distances_bytes[i] = distance / 4;
  }
}

