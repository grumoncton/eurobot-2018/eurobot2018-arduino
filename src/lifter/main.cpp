#include <DRV8825.h>
#include <Wire.h>
#include "address-map.h"
#include "table-side.h"

#define PRINT(name) (Serial.print(" " + (String) #name + ": " + (String) name))
#define MM_TO_STEPS(mm) (mm * 45.4545)


/**
 * Stepper setup
 */

// Pins
#define SPR (200)
#define RPM (600)
#define HOME_RPM (200)
#define ENA_PIN (10)
#define M0 (9)
#define M1 (8)
#define M2 (7)
#define STEP_PIN (6)
#define DIR_PIN (5)
#define MICRO_STEPS (16)
#define HOMING_PIN (12)

// Heights
int _currentHeight;
#define HOME_HEIGHT (56)
#define PICKUP_HEIGHT (59)
#define OVER_BASE_HEIGHT (20)
#define BLOCK_HEIGHT (58)
#define LATCH_HEIGHT (100)
#define SWITCH_HEIGHT (125)
#define MAX_HEIGHT (302)

DRV8825 lifter(SPR, DIR_PIN, STEP_PIN, ENA_PIN, M0, M1, M2);

enum Instruction {
  NONE = 0x00,
  GET_STATE = 0x01,
  SET_SIDE = 0x02,
  PICKUP = 0x10,
  SWITCH = 0x11,
  DROP = 0x20,
  OVER = 0x30,
};

enum State {
  UNINITIATED = -1,
  READY = 0,
  WORKING = 1,
  DONE = 3,
};

// Instruction variable used for actions
volatile Instruction _actionInstruction = NONE;

// Instruction used for data requests (GET_STATE, ...)
volatile Instruction _requestInstruction = NONE;

State _state = READY;

volatile uint8_t _targetPosition;

volatile TableSide _tableSide;

void move(int destination) {
  destination = constrain(destination, HOME_HEIGHT, MAX_HEIGHT);

  int16_t difference = destination - _currentHeight;

  lifter.enable();
  lifter.rotate(MM_TO_STEPS(difference));
  lifter.stop();
  lifter.disable();

  _currentHeight = destination;
}

void home() {
  lifter.enable();

  // Start non-blocking rotation
  lifter.startRotate(100 * 360);

  while (digitalRead(HOMING_PIN) == HIGH) {
    lifter.nextAction();
  }

  _currentHeight = HOME_HEIGHT;

  lifter.stop();
  lifter.disable();
}

void dataReceive(int bytes) {
  uint8_t message = Wire.read();

  if (message == SET_SIDE) {
    _tableSide = static_cast<TableSide>(Wire.read());
    return;
  }

  if (message == GET_STATE) {
    _requestInstruction = GET_STATE;
    return;
  }

  if (message == PICKUP || message == SWITCH) {
    _actionInstruction = static_cast<Instruction>(message);
    _state = WORKING;
    return;
  }

  if (message & DROP || message & OVER) {
    _actionInstruction = static_cast<Instruction>(message & 0xF0);
    _state = WORKING;

    // Position is least significant bits of message
    _targetPosition = message & 0x0F;
    return;
  }
}

void dataRequest() {
  if (_requestInstruction == GET_STATE) {
    Wire.write(_state);

    if (_state == DONE) {
      _state = READY;
    }
  }
}

void setup() {
  Wire.begin(ADDRESS_LIFER);
  Wire.onReceive(dataReceive);
  Wire.onRequest(dataRequest);


  // Setup stepper
  lifter.begin(RPM, MICRO_STEPS);
  lifter.disable();

  // Setup stepper homing pin
  pinMode(HOMING_PIN, INPUT_PULLUP);

  Serial.begin(9600);

  lifter.setRPM(HOME_RPM);
  home();

  move(LATCH_HEIGHT);
  lifter.setRPM(RPM);
}

void loop() {
  if (_state != WORKING) {
    return;
  }

  switch (_actionInstruction) {
    case PICKUP: {
      move(PICKUP_HEIGHT);
      _state = DONE;
      break;
    }

    case OVER: {
      move(OVER_BASE_HEIGHT + BLOCK_HEIGHT * _targetPosition);
      _state = DONE;
      break;
    }

    case DROP: {
      move(BLOCK_HEIGHT * _targetPosition);
      _state = DONE;
      break;
    }

    case SWITCH: {
      _state = DONE;
      move(SWITCH_HEIGHT);
      break;
    }

    default: {
      break;
    }
  }
}
