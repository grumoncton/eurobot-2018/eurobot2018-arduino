#include <Arduino.h>
#include <Servo.h>
#include <Wire.h>
#include "table-side.h"
#include "address-map.h"

enum Instruction {
  NONE = 0x00,
  GET_STATE = 0x01,
  SET_SIDE = 0x02,
  CLOSE_LATCH = 0x10,
  OPEN_LATCH = 0x11,
  CLOSE_ARM = 0x20,
  OPEN_ARM = 0x21,
  SORT_BALLS = 0x40,
};

enum State {
  UNINITIATED = -1,
  READY = 0,
  WORKING = 1,
  STOPPED = 2,
  DONE = 3,
};

volatile State _state = READY;

// Instruction variable used for actions (MOVE_TO, ROTATE_TO, ...)
volatile Instruction _actionInstruction = NONE;

// Instruction used for data requests (GET_STATE, GET_POS, ...)
volatile Instruction _requestInstruction = NONE;

// Side of the table we're on
volatile TableSide _tableSide = ORANGE;


//
// Positions
//

// Waste water dropper
#define LATCH_CLOSE (38)
#define LATCH_OPEN (128)
#define LATCH_HOME (0)

// Bee pushing arms
#define LEFT_ARM_CLOSED (152)
#define LEFT_ARM_EXTEND (20)
//#define LEFT_ARM_EXTEND (23)
#define RIGHT_ARM_CLOSED (40)
#define RIGHT_ARM_EXTEND (170)

// Mixed ball sorter
#define CATCH_GOOD (174)
#define CATCH_WASTE (86)

#define SORTER_DELAY (600)
#define WAIT_TIME (250)

Servo latch;
Servo leftArm;
Servo rightArm;
Servo sorter;

void dataReceive(int byte) {
  Instruction instruction = static_cast<Instruction>(Wire.read());

  switch(instruction) {

    case SET_SIDE: {
      _tableSide = static_cast<TableSide>(Wire.read());
      break;
    }

    case CLOSE_LATCH:
    case OPEN_LATCH:
    case CLOSE_ARM:
    case OPEN_ARM:
    case SORT_BALLS: {
      _actionInstruction = instruction;
      _state = WORKING;
    }

    default: {
      _requestInstruction = instruction;
      break;
    }
  }
}

void dataRequest() {
  switch (_requestInstruction) {
    case GET_STATE: {
      Wire.write(_state);

      if (_state == DONE) {
        _state = READY;
      }
    }

    default: {
    }
  }
}

// Set everything at starting position
void home() {
  latch.write(LATCH_CLOSE);
  leftArm.write(LEFT_ARM_CLOSED);

  // Wait for servos to finish moving
  delay(WAIT_TIME);
}

void setup() {
  Wire.begin(ADDRESS_SERVOS);
  Wire.onReceive(dataReceive);
  Wire.onRequest(dataRequest);

  latch.attach(12);
  leftArm.attach(10);
  rightArm.attach(11);
  sorter.attach(9);

  sorter.write(OPEN_LATCH);

  home();
  rightArm.write(RIGHT_ARM_CLOSED);
  leftArm.write(LEFT_ARM_CLOSED);
}

void loop() {
  if (_state != WORKING) {
    return;
  }

  switch (_actionInstruction) {

    // Dump wastewater
    case CLOSE_LATCH: {
      latch.write(LATCH_CLOSE);

      //Wait for servo to finish moving
      _state = DONE;
      break;
    }

    // Close latch so wastewater can't fall
    case OPEN_LATCH: {
      latch.write(LATCH_OPEN);

      // Wait for servo to finish moving
      delay(WAIT_TIME);
      _state = DONE;
      break;
    }

    // Starting / driving position of arm
    case CLOSE_ARM: {
      if (_tableSide == ORANGE) {
        leftArm.write(LEFT_ARM_CLOSED);
      } else {
        rightArm.write(RIGHT_ARM_CLOSED);
      }

      _state = DONE;
      break;
    }

    // Pushing position of arm
    case OPEN_ARM: {
      if (_tableSide == ORANGE) {
        leftArm.write(LEFT_ARM_EXTEND);
      } else {
        rightArm.write(RIGHT_ARM_EXTEND);
      }

      // Wait for servo to finish moving
      delay(WAIT_TIME);
      _state = DONE;
      break;
    }

    case SORT_BALLS: {
      for (uint8_t i = 0; i < 5; i++) {

        // Drop good ball and catch waste ball
        sorter.write(CATCH_GOOD);

        // Wait for next ball to fall
        delay(SORTER_DELAY);

        // Drop waste ball and catch good ball...
        sorter.write(CATCH_WASTE);
        delay(SORTER_DELAY);
      }

      sorter.write(CATCH_GOOD);
      delay(SORTER_DELAY);
      _state = DONE;
    }

    default: {
    }
  }
}
