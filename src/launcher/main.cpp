#include <DRV8825.h>
#include <Wire.h>
#include "address-map.h"
#include "table-side.h"

#define PRINT(name) (Serial.print((String) #name + ": " + (String) name + " "))

#define SPR (400)         //Steps Per Revolution
#define RPM (12)          //stepper RPM - Tested and works from 1 to 50rpm
#define ENA_PIN (10)      //Enable Pin
#define M0 (9)            //Mode 0
#define M1 (8)            //Mode 1
#define M2 (7)            //Mode 2
#define STEP_PIN (6)      //Step Pin
#define DIR_PIN (5)       //Direction Pin
#define MICRO_STEPS (16)  //microsteps

DRV8825 stepper(SPR, DIR_PIN, STEP_PIN, ENA_PIN, M0, M1, M2);

enum Instruction {
  NONE = 0x00,
  GET_STATE = 0x01,
  SET_SIDE = 0x02,
  LAUNCH_8 = 0x10,
  LAUNCH_4 = 0x11,
};

enum State {
  UNINITIATED = -1,
  READY = 0,
  WORKING = 1,
  STOPPED = 2,
  DONE = 3,
};

volatile State _state = UNINITIATED;

// Instruction variable used for actions (MOVE_TO, ROTATE_TO, ...)
volatile Instruction _actionInstruction = NONE;

// Instruction used for data requests (GET_STATE, GET_POS, ...)
volatile Instruction _requestInstruction = NONE;

// Side of the table we're on
volatile TableSide _tableSide = ORANGE;

void dataReceive(int bytes) {
  Instruction instruction = static_cast<Instruction>(Wire.read());

  switch(instruction) {

    case SET_SIDE: {
      _tableSide = static_cast<TableSide>(Wire.read());
      break;
    }

    case LAUNCH_4: {
      _actionInstruction = instruction;
      _state = WORKING;
      break;
    }

    case LAUNCH_8: {
      _actionInstruction = instruction;
      _state = WORKING;
      break;
    }

    default: {
      _requestInstruction = instruction;
      break;
    }
  }
}

void dataRequest() {
  switch (_requestInstruction) {
    case GET_STATE: {
      Wire.write(_state);

      if (_state == DONE) {
        _state = READY;
      }
      break;
    }

    default: {
    }
  }
}

void setup() {
  Serial.begin(9600);

  Wire.begin(ADDRESS_LAUNCHER);
  Wire.onReceive(dataReceive);
  Wire.onRequest(dataRequest);

  stepper.begin(RPM, MICRO_STEPS);
  stepper.disable();

  _state = READY;
  Serial.println("Ready");
}

void loop() {
  if (_state != WORKING) {
    return;
  }

  switch (_actionInstruction) {
    case LAUNCH_8 : {
      stepper.enable();
      stepper.rotate(90 * 10);
      stepper.disable();
      _state = DONE;
      break;
    }

    case LAUNCH_4 : {
      stepper.enable();
      stepper.rotate(90 * 5);
      stepper.disable();
      _state = DONE;
    }

    default: {
    }
  }
}

