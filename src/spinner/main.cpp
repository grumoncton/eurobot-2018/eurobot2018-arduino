#include <DRV8825.h>
#include <Servo.h>
#include <Wire.h>
#include "address-map.h"
#include "table-side.h"

enum Instruction {
  NONE = 0x00,
  GET_STATE = 0x01,
  SET_SIDE = 0x02,
  SERVO_ENABLE = 0x10,
  SERVO_DISABLE = 0x11,
  STEPPER_CW90 = 0x12,
  STEPPER_CCW90 = 0x13,
  SERVO_DOWN = 0x14,
};

enum State {
  UNINITIATED = -1,
  READY = 0,
  WORKING = 1,
  DONE = 3,
};

// Instruction variable used for actions
volatile Instruction _actionInstruction = NONE;

// Instruction used for data requests (GET_STATE, ...)
volatile Instruction _requestInstruction = NONE;

volatile State _state = READY;

volatile TableSide _tableSide;


/**
 * Stepper setup
 */

#define SPR (200)
#define RPM (800)
#define ENA_PIN (10)
#define M0 (9)
#define M1 (8)
#define M2 (7)
#define STEP_PIN (6)
#define DIR_PIN (5)
#define MICRO_STEPS (16)

DRV8825 stepper(SPR, DIR_PIN, STEP_PIN, ENA_PIN, M0, M1, M2);


/**
 * Servo setup
 */

#define SERVO_1_PIN 12
#define SERVO_2_PIN 11

#define SERVO_1_POS 120
#define SERVO_2_POS 60

#define SERVO_1_DOWN 140
#define SERVO_2_DOWN 40

Servo servo1;
Servo servo2;

void dataReceive(int bytes) {
  Instruction instruction = static_cast<Instruction>(Wire.read());

  switch (instruction) {
    case SET_SIDE: {
      _tableSide = static_cast<TableSide>(Wire.read());
      break;
    }

    case GET_STATE: {
      _requestInstruction = instruction;
      break;
    }

    default: {
      _actionInstruction = instruction;
      _state = WORKING;
    }
  }
}

void dataRequest() {
  if (_requestInstruction == GET_STATE) {
    Wire.write(_state);

    if (_state == DONE) {
      _state = READY;
    }
  }
}

void setup() {
  Wire.begin(ADDRESS_SPINNER);
  Wire.onReceive(dataReceive);
  Wire.onRequest(dataRequest);

  stepper.begin(RPM, MICRO_STEPS);

  // servo1.attach(SERVO_1_PIN);
  // servo1.write(SERVO_1_DOWN);
  // servo2.attach(SERVO_2_PIN);

  // Put servos in starting position
  // servo1.write(SERVO_1_POS);
  // servo2.write(SERVO_2_POS);

  // servo1.write(SERVO_1_HORIZONTAL);
  // servo2.write(SERVO_2_HORIZONTAL);
}

void loop() {
  if (_state != WORKING) {
    return;
  }

  switch (_actionInstruction) {
    case SERVO_DISABLE: {
      servo1.detach();
      servo2.detach();
      _state = DONE;
      break;
    }

    case SERVO_ENABLE: {
      servo1.attach(SERVO_1_PIN);
      servo2.attach(SERVO_2_PIN);
      servo1.write(SERVO_1_POS);
      servo2.write(SERVO_2_POS);
      _state = DONE;
      break;
    }

    case SERVO_DOWN: {
      servo1.attach(SERVO_1_PIN);
      servo2.attach(SERVO_2_PIN);
      servo1.write(SERVO_1_DOWN);
      servo2.write(SERVO_2_DOWN);
      delay(500);
      _state = DONE;
      break;
    }

    case STEPPER_CW90: {
      stepper.enable();
      stepper.rotate(1270);
      _state = DONE;
      break;
    }

    case STEPPER_CCW90: {
      stepper.enable();
      stepper.rotate(-1270);
      _state = DONE;
      break;
    }

    default: {
      break;
    }
  }
}

