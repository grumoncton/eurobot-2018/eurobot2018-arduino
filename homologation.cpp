#include <Arduino.h>
#include <RoboClaw.h>
#include <math.h>
#include <Wire.h>
#include "address-map.h"
#include "enums.h"
#include "table-side.h"

#define CONSTRAINED_BY(x, a, b) (a < x && x < b)
#define NUM_ULTRAS 8
#define ULTRA_TOLERANCE 200
#define ULTRA_EDGE_TOLERANCE 300
#define ULTRA_ANGLE_TOLERANCE 60
#define ULTRA_FACTOR 4
#define ULTRA_MAX_VALUE (1 << 8) * ULTRA_FACTOR


#define TO_DEGREES(a) (a * 180.0 / PI)
#define TO_RADIANS(a) (a / 180.0 * PI)

#define PRINTA(name) (Serial.print(" " + (String) #name + ": " + (String) TO_DEGREES(name)))

//Setting up communication with RoboClaw on Serial3 with 10ms timeout
RoboClaw roboclaw(&Serial2, 10000);

//Setting up communication with Serial

#define PRINT(name) (Serial.print(" " + (String) #name + ": " + (String) name))

#define ROBOCLAW_DELAY 100

//Robot Dimensions
double MM_PER_CNT_INT_L = (WHEEL_DIA_INT_L*PI)/CPR_INT; //Internal encoder millimeters per count
double MM_PER_CNT_INT_R = (WHEEL_DIA_INT_R*PI)/CPR_INT; //Internal encoder millimeters per count
double MM_PER_CNT_EXT = (WHEEL_DIA_EXT*PI)/CPR_EXT; //External encoder millimeters per count

//Movement quantities converted speed in counts/s and accel in counts/s^2
double DistSpeedL = DISTSPEED/MM_PER_CNT_INT_L;
double DistAccelL = DISTACCEL/MM_PER_CNT_INT_L;
double DistDecelL = DISTDECEL/MM_PER_CNT_INT_L;
double RotateSpeedL = ROTATESPEED/MM_PER_CNT_INT_L;
double RotateAccelL = ROTATEACCEL/MM_PER_CNT_INT_L;
double RotateDecelL = ROTATEDECEL/MM_PER_CNT_INT_L;
double DistSpeedR = DISTSPEED/MM_PER_CNT_INT_R;
double DistAccelR = DISTACCEL/MM_PER_CNT_INT_R;
double DistDecelR = DISTDECEL/MM_PER_CNT_INT_R;
double RotateSpeedR = ROTATESPEED/MM_PER_CNT_INT_R;
double RotateAccelR = ROTATEACCEL/MM_PER_CNT_INT_R;
double RotateDecelR = ROTATEDECEL/MM_PER_CNT_INT_R;

//Robot odometry variables
double XPos = X_START;
double YPos = Y_START;
double APos = A_START;
double APosRad = TO_RADIANS(A_START);

//External Encoder Parameters
int EncoderPinL1 = 2; //Left encoder's green wire
int EncoderPinL2 = 3; //Left encoder's white wire
int EncoderPinR1 = 18; //Right encoder's white wire
int EncoderPinR2 = 19; //Right encoder's green wire
volatile long EncoderValueL;
volatile long EncoderValueR;

//External Encoder Position Variables
long LastEncoderValueLRead;
long LastEncoderValueRRead;

//Robot odometry variables based on external encoders
double XPosExt = XPos;
double YPosExt = YPos;
double APosExt = APosRad;
double APosExtRel = APosExt;

bool Collision;
bool PrecisionCorrection = false;

double AMaxError = TO_RADIANS(3.0);
double XMaxError = 5;
double YMaxError = 5;

#define AccelPin A2
uint8_t AccelCnt;
#define MaxAccelCnt 60

#define DistanceArrayLength 5 //Running average length during distance command
#define RotateArrayLength 5 //Running average length during rotation command
bool DistanceBool = false;
volatile uint8_t UpdateCnt;
uint8_t MaxPulseDist = 25; //Update position during distance command after this many counts
uint8_t MaxPulseRotate = 25; //Update position during rotation command after this many counts
uint8_t ReadBufferDelay = 100; //Read RoboClaw every X milliseconds

struct Coord {
  double x;
  double y;
};

Direction _currentDirection;

// Global variables used for `MOVE_TO` instruction
Coord _moveToTarget;
Direction _moveToDirection;

// Global variable used for `ROTATE_TO` instruction
double _targetAngle = 0;

// Global variables used for `MOVE_TO_PATH` instruction
Coord path[8];
uint8_t _pathIndex = 0;
uint8_t _pathLength = 0;

volatile uint8_t _ultraDistances[NUM_ULTRAS];

// Instruction variable used for actions (MOVE_TO, ROTATE_TO, ...)
volatile Instruction _actionInstruction = NONE;

// Instruction used for data requests (GET_STATE, GET_POS, ...)
volatile Instruction _requestInstruction = NONE;

// Internal state of the robot
volatile State _state = READY;

// Side of the table we're on
volatile TableSide _tableSide = ORANGE;

//Function decleration
void MoveTo(double x, double y, double a, Direction dir, bool ajust);
void AdjustmentTurn(double deltaA);
void UpdateExtPos();
void UpdateExtEncoderL();
void UpdateExtEncoderR();
void ReadIntEnc();


bool shouldStop() {
  if (_actionInstruction != STOP) {
    return false;
  }

  // If we're not close to a wall, don't ignore anything
  // (return true right away)
  if (
    XPos > ULTRA_EDGE_TOLERANCE &&
    YPos > ULTRA_EDGE_TOLERANCE &&
    XPos < 2000 - ULTRA_EDGE_TOLERANCE &&
    YPos < 3000 - ULTRA_EDGE_TOLERANCE
  ) {
    return true;
  }

  // Check each ultra
  for (uint8_t i = 0; i < NUM_ULTRAS; i++) {

    // Ignore useless side ultras
    if (i == 2 || i == 6) {
      continue;
    }

    if (_currentDirection == BACKWARDS && (i == 7 || i == 0 || i == 1)) {
      continue;
    }

    if (_currentDirection == FORWARDS && (i == 3 || i == 4 || i == 5)) {
      continue;
    }

    uint8_t dist = _ultraDistances[i];

    // If ultra isn't blocked, skip it
    if (dist > ULTRA_TOLERANCE) {
      continue;
    }

    uint16_t blockedUltraAngle = i * 360 / NUM_ULTRAS;
    uint16_t obstacleAngle = ((uint16_t) APos + blockedUltraAngle) % 360;

    // Check if ultra should be ignored because it's close to an edge and the angle it's blocked at is pointing at the wall
    if (!(XPos < ULTRA_EDGE_TOLERANCE && CONSTRAINED_BY(obstacleAngle, 180 - ULTRA_ANGLE_TOLERANCE, 180 + ULTRA_ANGLE_TOLERANCE))) {
      return true;
    }

    if (!(YPos < ULTRA_EDGE_TOLERANCE && CONSTRAINED_BY(obstacleAngle, 270 - ULTRA_ANGLE_TOLERANCE, 270 + ULTRA_ANGLE_TOLERANCE))) {
      return true;
    }

    if (!(XPos > (2000 - ULTRA_EDGE_TOLERANCE) && CONSTRAINED_BY(obstacleAngle, 360 - ULTRA_ANGLE_TOLERANCE, 0 + ULTRA_ANGLE_TOLERANCE))) {
      return true;
    }

    if (!(YPos > (3000 - ULTRA_EDGE_TOLERANCE) && CONSTRAINED_BY(obstacleAngle, 90 - ULTRA_ANGLE_TOLERANCE, 90 + ULTRA_ANGLE_TOLERANCE))) {
      return true;
    }
  }

  // If we get through all the ultras and all of them are not blocked or should be ignored, return false
  return false;
}

// Stop robot and update states
// We have to stop immediatly (not in `void loop`). Otherwise, if the robot is in a moveTo, it won't stop.
bool checkShouldStop() {

  // If we don't have to stop, return
  if (!shouldStop()) {
    return false;
  }

  Serial.println("\n\nStopping!!!!\n\n");

  // Stop motors
  roboclaw.SpeedAccelDistanceM1M2(ADDRESS, 5000, 0, 200, 0, 200, 1);

  // Update state
  _state = STOPPED;
  _actionInstruction = NONE;
  return true;
}

void receiveHandler(int bytes) {
  Instruction instruction = static_cast<Instruction>(Wire.read());

  switch(instruction) {

    // case RESUME: {
      // _state = READY;
      // break;
    // }

    case STOP: {
      for (uint8_t i = 0; i < NUM_ULTRAS; i++) {
        _ultraDistances[i] = Wire.read() * ULTRA_FACTOR;
      }

      _actionInstruction = STOP;
      break;
    }

    case SET_SIDE: {
      _tableSide = static_cast<TableSide>(Wire.read());
      break;
    }

    // For the rest of the instructions, `moveTo` is called in `void loop` as `checkShouldStop` is called in `moveTo` loop

    // Read destination when `MOVE_TO` received
    case MOVE_TO: {
      _moveToDirection = static_cast<Direction>(Wire.read());
      uint16_t xBytes = (Wire.read() << 8) | Wire.read();
      uint16_t yBytes = (Wire.read() << 8) | Wire.read();
      _moveToTarget.x = (double) xBytes / (double) 20.0;
      _moveToTarget.y = (double) yBytes / (double) 20.0;
      _actionInstruction = MOVE_TO;
      _state = WORKING;
      break;
    }

    case MOVE_TO_RELATIVE: {
      _moveToDirection = static_cast<Direction>(Wire.read());
      int32_t xBytes = (Wire.read() << 8) | Wire.read();
      int32_t yBytes = (Wire.read() << 8) | Wire.read();

      if (bitRead(xBytes, 15)) { xBytes -= (1 << 16); }
      if (bitRead(yBytes, 15)) { yBytes -= (1 << 16); }

      double diffX = (double) xBytes / (double) 20.0;
      double diffY = (double) yBytes / (double) 20.0;

      _moveToTarget.x = XPos + diffX * cos(APosRad) + diffY * sin(APosRad);
      _moveToTarget.y = YPos + diffX * sin(APosRad) + diffY * cos(APosRad);
      _actionInstruction = MOVE_TO;
      _state = WORKING;
      break;
    }

    // Read target angle when `ROTATE_TO` received
    case ROTATE_TO: {
      _targetAngle = ((double) Wire.read()) / 255.0 * 360.0;
      _actionInstruction = instruction;
      _state = WORKING;
      break;
    }

    // Read path when MOVE_TO_PATH received
    case MOVE_TO_PATH: {
      _pathIndex = 0;
      _pathLength = bytes / 4;

      for (uint8_t i = 0; i < _pathLength; i++) {
        Coord coord;

        uint16_t xBytes = (Wire.read() << 8) | Wire.read();
        uint16_t yBytes = (Wire.read() << 8) | Wire.read();
        coord.x = (double) xBytes / (double) 20.0;
        coord.y = (double) yBytes / (double) 20.0;

        path[i] = coord;
      }
      _actionInstruction = instruction;
      _state = WORKING;
      break;
    }

    default: {
      _requestInstruction = instruction;
      break;
    }
  }
}

void requestHandler() {
  switch(_requestInstruction) {

    // Send position
    case GET_POS: {
      // Serial.println("Sending position");
      byte result[5] = {0};
      uint16_t XPosInt = (uint16_t) (XPos * 20);
      uint16_t YPosInt = (uint16_t) (YPos * 20);
      result[0] = XPosInt >> 8;
      result[1] = XPosInt % 256;
      result[2] = YPosInt >> 8;
      result[3] = YPosInt % 256;
      result[4] = APosRad * 255.0 / (2.0 * PI);
      for (uint8_t i = 0; i < 5; i++) {
        Wire.write(result[i]);
      }
      _actionInstruction = NONE;
      break;
    }

    case GET_STATE: {
      Wire.write(_state);

      // Change state to `READY` after we send `DONE`
      // This way, the pi doesn't think we're done the next task too
      if (_state == DONE || _state == DONE_PATH_SEGMENT) {
        _state = READY;
      }
    }
    default: {}
  }
}

void setup() {
  //Set baud rate to be communicated with RoboClaw (Must be set in RoboClaw parameters)
  Serial.begin(9600);
  roboclaw.begin(38400);
  roboclaw.ResetEncoders(ADDRESS); //Reset internal encoders to 0
  delay(ROBOCLAW_DELAY); //Always allow 100ms delay after resetting encoders
  // Serial.print("Current Theoretical Position: 0, 0, 0!");
  // Serial.print("================================================================================================!");
  // Serial.print("Initialized!");
  // Serial.print("================================================================================================!");

  //Accelerometer pin
  pinMode(AccelPin, INPUT);

  Wire.begin(ADDRESS_WHEELS);
  Wire.onReceive(receiveHandler);
  Wire.onRequest(requestHandler);

  //External Encoder Pin Setup
  pinMode(EncoderPinL1, INPUT);
  pinMode(EncoderPinL2, INPUT);
  pinMode(EncoderPinR1, INPUT);
  pinMode(EncoderPinR2, INPUT);
  digitalWrite(EncoderPinL1, HIGH); //Turn pullup resistor on
  digitalWrite(EncoderPinL2, HIGH); //Turn pullup resistor on
  digitalWrite(EncoderPinR1, HIGH); //Turn pullup resistor on
  digitalWrite(EncoderPinR2, HIGH); //Turn pullup resistor on
  attachInterrupt(digitalPinToInterrupt(EncoderPinL1), UpdateExtEncoderL, RISING);
  attachInterrupt(digitalPinToInterrupt(EncoderPinR1), UpdateExtEncoderR, RISING);

  Serial.print("Ready");
  // Serial.print("!");
}

void loop() {
  // MoveTo(_moveToTarget.x, _moveToTarget.y, -1, _moveToDirection, false);
  checkShouldStop();

  switch(_actionInstruction) {
    case MOVE_TO: {
      if (_state == WORKING) {
        Serial.print("Starting move to.");
        // Serial.print("!");
        PRINT(_moveToTarget.x);
        PRINT(_moveToTarget.y);
        PRINT(_moveToDirection);
        Serial.println();
        // Serial.print("!");
        MoveTo(_moveToTarget.x, _moveToTarget.y, -1, _moveToDirection, false);
        Serial.print("After move to");
        PRINT(XPos);
        PRINT(APos);
        PRINT(TO_DEGREES(APosRad));
        Serial.println();
        // Serial.print("!");
        _state = DONE;
      }
      return;
    }

    case ROTATE_TO: {
      if (_state == WORKING) {
        Serial.print("Starting rotate to.");
        // Serial.print("!");
        PRINT(_targetAngle);
        Serial.println();
        // Serial.print("!");
        MoveTo(XPos, YPos, _targetAngle, FORWARDS, false);
        _state = DONE;
      }
      return;
    }

    case MOVE_TO_PATH: {
      if (_state == WORKING && _pathIndex >= _pathLength) {
        _state = DONE_PATH_SEGMENT;
        return;
      }

      if (_state == WORKING) {
        Serial.print("Starting move to path");
        // Serial.print("!");
        Serial.println();
        Coord dest = path[_pathIndex];
        PRINT(dest.x);
        PRINT(dest.y);
        // Serial.print("!");
        Serial.println();
        _pathIndex++;
        MoveTo(dest.x, dest.y, -1, FORWARDS, false);
        Serial.print("Done move to. Final destination:");
        Serial.println();
        // Serial.print("!");
        PRINT(XPos);
        PRINT(YPos);
        // Serial.print("!");
        Serial.println();
      }
      return;
    }

    default: {
    }
  }
}

//x and y are destination coordinates
//a is the robot's orientation to be adjusted to once the coordinates are reached (-1 for no adjustment angle)
//start decides which motors to be engaged during first AdjustmentTurn() (1 = Left, 2 = Right, 3 = Simultaneous)
//end decides which motors to be engaged during second AdjustmentTurn() (1 = Left, 2 = Right, 3 = Simultaneous)
//dir set to 0 for forward movement, 1 for backwards movement
void MoveTo(double x, double y, double a, Direction dir, bool adjust) {
  _currentDirection = dir;

  do {
    //Movement variables
    double xDist; //X Distance in mm to be executed during MoveTo command
    double xDistCountIntL; //X Distance in counts to be executed during MoveTo command
    double xDistCountIntR; //X Distance in counts to be executed during MoveTo command
    double yDist; //Y Distance in mm to be executed during MoveTo command
    double yDistCountIntL; //Y Distance in counts to be executed during MoveTo command
    double yDistCountIntR; //Y Distance in counts to be executed during MoveTo command
    double travelDistL; //Distance to be accomplished during MoveTo in counts
    double travelDistR; //Distance to be accomplished during MoveTo in counts
    double adjustedA; //Angle to be oriented to in radians
    double deltaA; //Angle between current position and adjustedA in radians

    double aError;
    double xError;
    double yError;

    double adjustmentDir = false; //Set dir to 1 temporarily if adjustment needs to move in reverse, dir will be reset to 0 at end of loop

    double aRad = PI*(a/180);

    PrecisionCorrection = false;
    Collision = false;

    xDist = x-XPos;
    xDistCountIntL = xDist/MM_PER_CNT_INT_L;
    xDistCountIntR = xDist/MM_PER_CNT_INT_R;
    yDist = y-YPos;
    yDistCountIntL = yDist/MM_PER_CNT_INT_L;
    yDistCountIntR = yDist/MM_PER_CNT_INT_R;

    //Adjustment angle calculations with resulting angle between 0 and 360 degrees (valid for both games)
    if (((xDist > 0 && yDist >= 0) && (dir == FORWARDS)) || ((xDist < 0 && yDist <= 0) && (dir == BACKWARDS)))
    {
      adjustedA = atan2(fabs(yDist), fabs(xDist));
    }
    else if (((xDist <= 0 && yDist > 0) && (dir == FORWARDS)) || ((xDist >= 0 && yDist < 0) && (dir == BACKWARDS)))
    {
      adjustedA = atan2(fabs(xDist), fabs(yDist))+PI/2;
    }
    else if (((xDist < 0 && yDist <= 0) && (dir == FORWARDS)) || ((xDist > 0 && yDist >= 0) && (dir == BACKWARDS)))
    {
      adjustedA = atan2(fabs(yDist), fabs(xDist))+PI;
    }
    else if (((xDist >= 0 && yDist < 0) && (dir == FORWARDS)) || ((xDist <= 0 && yDist > 0) && (dir == BACKWARDS)))
    {
      adjustedA = atan2(fabs(xDist), fabs(yDist))+(3*PI)/2;
    }
    else if ((xDist == 0) && (yDist == 0))
    {
      adjustedA = APosRad;
    }

    deltaA = adjustedA-APosRad;

    if ((adjust) && (fabs(xDist) < 100) && (fabs(yDist) < 100) && ((deltaA > PI/2) || (deltaA < -PI/2)))
    {
      dir = FORWARDS;
      adjustmentDir = true;

      if (((xDist > 0 && yDist >= 0) && (dir == FORWARDS)) || ((xDist < 0 && yDist <= 0) && (dir == BACKWARDS)))
      {
        adjustedA = atan2(fabs(yDist), fabs(xDist));
      }
      else if (((xDist <= 0 && yDist > 0) && (dir == FORWARDS)) || ((xDist >= 0 && yDist < 0) && (dir == BACKWARDS)))
      {
        adjustedA = atan2(fabs(xDist), fabs(yDist))+PI/2;
      }
      else if (((xDist < 0 && yDist <= 0) && (dir == FORWARDS)) || ((xDist > 0 && yDist >= 0) && (dir == BACKWARDS)))
      {
        adjustedA = atan2(fabs(yDist), fabs(xDist))+PI;
      }
      else if (((xDist >= 0 && yDist < 0) && (dir == FORWARDS)) || ((xDist <= 0 && yDist > 0) && (dir == BACKWARDS)))
      {
        adjustedA = atan2(fabs(xDist), fabs(yDist))+(3*PI)/2;
      }
      else if ((xDist == 0) && (yDist == 0))
      {
        adjustedA = APosRad;
      }

      deltaA = adjustedA-APosRad;
    }

    if (deltaA != 0)
    {
      AdjustmentTurn(deltaA); //First adjustment turn to align robot with destination coordinate
      APosRad = APosExtRel;
      XPos = XPosExt;
      YPos = YPosExt;

      roboclaw.ResetEncoders(ADDRESS);
      delay(ROBOCLAW_DELAY);
    }

    travelDistL = hypot(xDistCountIntL, yDistCountIntL);
    travelDistR = hypot(xDistCountIntR, yDistCountIntR);

    Serial.println("Done ajustment turn");
    if ((travelDistL != 0 && !Collision) && (travelDistR != 0 && !Collision))
    {
      if (dir == FORWARDS) //Forward movement
      {
        roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, DistAccelR, DistSpeedR, DistDecelR, travelDistR, DistAccelL, DistSpeedL, DistDecelL, travelDistL, 0);
      }
      else //Backwards movement
      {
        roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, DistAccelR, DistSpeedR, DistDecelR, -travelDistR, DistAccelL, DistSpeedL, DistDecelL, -travelDistL, 0);
      }
      Serial.println("Done roboclaw thing");

      DistanceBool = true;
      UpdateExtPos();
      DistanceBool = false;

      roboclaw.ResetEncoders(ADDRESS);
      delay(ROBOCLAW_DELAY);

      XPos=XPosExt;
      YPos=YPosExt;
      APosRad = APosExtRel;

    }
    if (a == -1) //No adjustment angle (same as arriving orientation)
    {
      deltaA = 0;
      a = 180*(adjustedA/PI);
    }
    else
    {
      deltaA = aRad-APosRad;
    }

    if (deltaA != 0 && !Collision)
    {
      AdjustmentTurn(deltaA); //Second adjustment turn to align robot with final angle argument in MoveTo()
      XPos = XPosExt;
      YPos = YPosExt;
      APosRad = APosExtRel;

      roboclaw.ResetEncoders(ADDRESS);
      delay(ROBOCLAW_DELAY);
    }

    if (adjustmentDir)
    {
      dir = BACKWARDS;
    }

    if (adjust)
    {
      aError = APosRad - (a/180)*PI;
      if (aError > PI)
      {
        aError -= 2*PI;
      }
      else if (aError < -PI)
      {
        aError += 2*PI;
      }
      if (fabs(aError) > AMaxError)
      {
        PrecisionCorrection = true;
        // Serial.print("Rotation precision error ");
        // Serial.print(aError);
        // Serial.print("!");
      }

      xError = XPos-x;
      yError = YPos-y;
      if (fabs(xError) > XMaxError)
      {
        PrecisionCorrection = true;
      }
      if (yError > YMaxError)
      {
      }
    }
  }
  while (PrecisionCorrection);
  Serial.print("Current Theoretical Position: X ");
  Serial.print(x);
  Serial.print(", Y ");
  Serial.print(y);
  Serial.print(", A ");
  Serial.print(a);
  Serial.println();
  Serial.print("Current Position Based on ExtEnc: X ");
  Serial.print(XPosExt);
  Serial.print(", Y ");
  Serial.print(YPosExt);
  Serial.print(", ARel ");
  Serial.print((APosExtRel/PI)*180);
  Serial.println();
  Serial.print("------------------------------------------------------------------------------------------------");
  Serial.println();
  Serial.print("Position reached");
  Serial.println();
  Serial.print("------------------------------------------------------------------------------------------------");
  Serial.println();
}

void AdjustmentTurn(double deltaA)
{
  double turnDistL; //Distance to be accomplished by each wheel while turning
  double turnDistR; //Distance to be accomplished by each wheel while turning

  //Conditions to determine smallest angle to be accomplished (between -PI and PI)
  if (deltaA > PI)
  {
    deltaA -= 2*PI;
  }
  else if (deltaA < -PI)
  {
    deltaA += 2*PI;
  }

    turnDistL = (PI*WHEEL_SEP_INT*(fabs(deltaA)/(2*PI)))/MM_PER_CNT_INT_L;
    turnDistR = (PI*WHEEL_SEP_INT*(fabs(deltaA)/(2*PI)))/MM_PER_CNT_INT_R;

  Serial.print("Before roboclaw");
  Serial.println();
  //Turn achieved by position command with distance arguments having opposite signs to each other
  if (((deltaA >= 0 && _tableSide == GREEN) || (deltaA < 0 && _tableSide == ORANGE)))//Positive rotation based on x axis (CCW)
  {
    roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, RotateAccelR, RotateSpeedR, RotateDecelR, 0, RotateAccelL, RotateSpeedL, RotateDecelL, 0, 0);
    roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, RotateAccelR, RotateSpeedR, RotateDecelR, turnDistR, RotateAccelL, RotateSpeedL, RotateDecelL, -turnDistL, 0);
  }
  else //Negative rotation based on x axis (CW)
  {
    roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, RotateAccelR, RotateSpeedR, RotateDecelR, 0, RotateAccelL, RotateSpeedL, RotateDecelL, 0, 0);
    roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, RotateAccelR, RotateSpeedR, RotateDecelR, -turnDistR, RotateAccelL, RotateSpeedL, RotateDecelL, turnDistL, 0);
  }
  Serial.print("After roboclaw");
  Serial.println();

  UpdateExtPos();
  Serial.print("After UpdateExtPos");
  Serial.println();
}

void UpdateExtPos()
{
  //Initiate and reset encoder variables
  double leftDist = 0; //Distance traveled by left motor since last updated
  double rightDist = 0; //Distance traveled by right motor since last updated
  double centerDist = 0; //Distance traveled by the center of the robot since last updated
  double arcA = 0; //Arc accomplished by the robot since last updated where the angle is based on the axle of the robot at starting angle
  long encoderValueLRead = 0; //Current reading variable of the left external encoder
  long encoderValueRRead = 0; //Current reading variable of the right external encoder
  long LastEncoderValueLRead = 0;
  long LastEncoderValueRRead = 0;
  EncoderValueL = 0; //Reset global EncoderValueL attached to interrupt
  EncoderValueR = 0; //Reset global EncoderValueR attached to interrupt

  //Distance running average variables
  double arcTotal = 0;
  double arcs[DistanceArrayLength] = {0};
  int index = 0;
  double arcAverage = 0;

  //Rotate running average variables
  double xDisp = 0;
  double yDisp = 0;
  double xDispTotal = 0;
  double yDispTotal = 0;
  double xDispArr[RotateArrayLength] = {0};
  double yDispArr[RotateArrayLength] = {0};
  double xDispAverage = 0;
  double yDispAverage = 0;

  // Acceleration variables
  int accel = 0;
  bool firstMeasure = true;
  int lastAccel;
  int jerk = 0;

  int32_t sameValueL = 0;
  int32_t sameValueR = 0;
  uint8_t depth1 = 0, depth2 = 0;
  //Depth 1 is motor 1 buffer value, depth 2 is motor 2 buffer value
  //Buffer value is the number of commands waiting to be executed by RoboClaw
  //If buffer is 0, last command is currently being executed, and 0x80 means buffer is empty
  Serial.print("Before Buffer Read");
  Serial.print("!");
  while (depth1 != 0x80 && depth2 != 0x80) //Loop until position command is completed
  {
    bool shouldStop = checkShouldStop();

    // if (shouldStop) {
      // return;
    // }

    roboclaw.ReadBuffers(ADDRESS, depth1, depth2);

    uint32_t start = millis();
    uint32_t end = millis();
    while ((end - start) < ReadBufferDelay)
    {
      if (AccelCnt >= MaxAccelCnt)
      {
        accel = analogRead(AccelPin);
        if (firstMeasure)
        {
          lastAccel = accel;
          firstMeasure = false;
        }
        jerk = abs(accel-lastAccel);
        if (jerk > MAXJERK)
        {
          roboclaw.DutyM1M2(ADDRESS, 0, 0);
          Collision = true;
          Serial.print("Collision detected, jerk measured ");
          Serial.print(jerk);
          Serial.print('!');
          delay(5000);
          break;
        }
        lastAccel = accel;
        AccelCnt = 0;
      }

      if (((UpdateCnt >= MaxPulseDist) && DistanceBool) || ((UpdateCnt >= MaxPulseRotate) && !DistanceBool))
      {
        encoderValueLRead = EncoderValueL; //Store left encoder reading as a variable
        encoderValueRRead = EncoderValueR; //Store right encoder reading as a variable
        leftDist = (encoderValueLRead-LastEncoderValueLRead)*MM_PER_CNT_EXT;
        rightDist = (encoderValueRRead-LastEncoderValueRRead)*MM_PER_CNT_EXT;
        centerDist = ((leftDist+rightDist)/2);
        if (_tableSide == ORANGE)
        {
          arcA = (leftDist-rightDist)/WHEEL_SEP_EXT;
        }
        else
        {
          arcA = (rightDist-leftDist)/WHEEL_SEP_EXT;
        }

        if (DistanceBool)
        {
          arcTotal -= arcs[index];
          arcs[index] = arcA;
          arcTotal += arcs[index];
          index++;
          if (index >= DistanceArrayLength)
          {
            index = 0;
          }
          arcAverage = arcTotal/DistanceArrayLength;
          if (fabs(arcA) > 0.8*fabs(arcAverage))
          {
            arcA = 0;
          }
        }

        if (arcA != 0)
        {
          xDisp = (centerDist/arcA)*(sin(APosExt + arcA)-sin(APosExt));
          yDisp = (centerDist/arcA)*(cos(APosExt)-cos(APosExt+arcA));
        }
        else
        {
          xDisp = centerDist*cos(APosExt); //Approximation valid  for a small arcA
          yDisp = centerDist*sin(APosExt); //Approximation valid  for a small arcA
        }

        if (!DistanceBool)
        {
          xDispTotal -= xDispArr[index];
          yDispTotal -= yDispArr[index];
          xDispArr[index] = xDisp;
          yDispArr[index] = yDisp;
          xDispTotal += xDispArr[index];
          yDispTotal += yDispArr[index];
          index++;
          if (index >= RotateArrayLength)
          {
            index = 0;
          }
          xDispAverage = xDispTotal/RotateArrayLength;
          yDispAverage = yDispTotal/RotateArrayLength;
          if (fabs(xDisp) > fabs(xDispAverage))
          {
            xDisp = 0;
          }
          if (fabs(yDisp) > fabs(yDispAverage))
          {
            yDisp = 0;
          }
        }

        XPosExt += xDisp;
        YPosExt += yDisp;

        APosExt += arcA;

        //Conditions to determine smallest angle
        if (APosExt > PI)
        {
          APosExt = -2*PI+APosExt;
        }
        else if (APosExt < -PI)
        {
          APosExt = 2*PI+APosExt;
        }

        //Convert angle relative to axle to the global coordinates
        if (APosExt < 0)
        {
          APosExtRel = 2*PI+APosExt;
        }
        else
        {
          APosExtRel = APosExt;
        }

      }

      if (LastEncoderValueLRead == encoderValueLRead)
      {
        sameValueL++;
      }
      else
      {
        sameValueL = 0;
      }

      if (LastEncoderValueRRead == encoderValueRRead)
      {
        sameValueR++;
      }
      else
      {
        sameValueR = 0;
      }

      //Save encoder values for next update
      LastEncoderValueLRead = encoderValueLRead;
      LastEncoderValueRRead = encoderValueRRead;

      // if ((sameValueL >= 1000) || (sameValueR >= 1000))
      // {
        // depth1 = 0x80;
        // depth2 = 0x80;
        // break;
      // }

      UpdateCnt = 0;

      end = millis();
    }
  }
  Serial.println("After Buffer Read");
}

void UpdateExtEncoderL()
{
    if (digitalRead(EncoderPinL2) == LOW)
    {
        EncoderValueL++;
    }
    else
    {
        EncoderValueL--;
    }
    UpdateCnt++;
    AccelCnt++;
}

void UpdateExtEncoderR()
{
    if (digitalRead(EncoderPinR2) == LOW) 
    {
        EncoderValueR++;
    }
    else
    {
        EncoderValueR--;
    }
    UpdateCnt++;
    AccelCnt++;
}

