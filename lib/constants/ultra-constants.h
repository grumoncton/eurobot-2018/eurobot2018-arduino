#define NUM_ULTRAS 8
#define ULTRA_TOLERANCE 200
#define ULTRA_EDGE_TOLERANCE 300
#define ULTRA_ANGLE_TOLERANCE 60
#define ULTRA_FACTOR 2
#define ULTRA_MAX_VALUE (1 << 8) * ULTRA_FACTOR

